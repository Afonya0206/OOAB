﻿$(document).ready(function () {

    $('#DynamicTypes').find(':first-child').attr('checked', true);

    $('#AddPhoneButton').click(function (event) {
        addPhoneField();
        return false;
    });

    $('#AddTypeButton').click(function (event) {
        addTypeField();
        return false;
    });

    $('#AddOptionButton').click(function (event) {
        addOptionField();
        return false;
    });

    //Для удаления первого поля
    $('.DeleteDynamicExtraField').click(function (event) {
        $(this).parent().remove();
        return false;
    });

    $(document).ready(function () {
        $('input:radio[id=RadioButtonOption]:first').attr('checked', true);
    });

});

function addOptionField() {

    var div = $('<div/>', {
        'class': 'DynamicExtraField',
        'id': 'DynamicExtraField'
    }).insertBefore($("#AddOptionButton"));
    var label = $('<label/>').html("Варіант відповіді:").appendTo(div);
    var br = $('<br/>').appendTo(div);
    var input = $('<input/>', { name: 'opt' }).appendTo(div);
    var input = $('<input/>', {
        value: 'Видалити',
        type: 'button',
        'class': 'DeleteDynamicExtraField'
    }).appendTo(div);
    input.click(function () {
        input.parent().remove();
    });
}

function addPhoneField() {

    var div = $('<div/>', {
        'class': 'DynamicExtraField',
        'id' : 'DynamicExtraField'
    }).insertBefore($("#AddPhoneButton"));
    var label = $('<label/>').html("Телефон:").appendTo(div);
    var br = $('<br/>').appendTo(div);
    var input = $('<input/>', { name: 'phones' }).appendTo(div);
    var input = $('<input/>', {
            value: 'Видалити',
            type: 'button',
            'class': 'DeleteDynamicExtraField'
    }).appendTo(div);
    input.click(function () {
        input.parent().remove();
    });
}


function addTypeField() {
    var div = $('<div/>', {
        'class': 'DynamicTypes',
        'id': 'DynamicTypes'
    }).insertBefore($("#AddTypeButton"));
    var inputradio = $('<input/>', {
        type: 'radio',
        'id':'radiotype',
        'name': 'type_owner',
        'checked': 'checked',
        value: 'new'
    }).appendTo(div);
    var input = $('<input/>', {
        type: 'text',
        'id': 'texttype',
        'name': 'newvalue'
    }).appendTo(div);
    var inputdelete = $('<input/>', {
        value: 'Видалити',
        type: 'button',
        'class': 'DeleteDynamicExtraField'
    }).appendTo(div);
    $('#AddTypeButton').remove();

    inputdelete.click(function () {    
        $(this).parent().remove();
        if (inputradio.prop("checked")) {
            $('div#DynamicTypes').find(':first-child').prop("checked", true);
        }
        
        var inputnew = $('<input/>', {
            value: 'Додати посаду',
            type: 'button',
            'id': 'AddTypeButton'
        }).appendTo($("#DynamicTypesContainer"));
        inputnew.click(function () {
            addTypeField();
        });        
    });

}