﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Universal_OSBB.Models.Entities;
using Universal_OSBB.Models.Entities.Reports;

namespace Universal_OSBB.Models.Context
{
    public class OOABInitializer : DropCreateDatabaseIfModelChanges <OOABContext>
    {
        protected override void Seed(OOABContext context)
        {
            List<OOAB> OOABs = new List<OOAB>
            {
                new OOAB { Name = "Валентинівський" }
            };

            OOABs.ForEach(item => context.OOABs.Add(item));
            context.SaveChanges();

            List<Measurement> Measurements = new List<Measurement>
            {
                new Measurement { Name = "шт"},
                new Measurement { Name = "кг"},
                new Measurement { Name = "упак."},
                new Measurement { Name = "м"},
                new Measurement { Name = "куб. м"},
                new Measurement { Name = "кв. м"}
            };
            Measurements.ForEach(item => context.Measurements.Add(item));
            context.SaveChanges();

            List<Cost> costs = new List<Cost>
            {
                new Cost {
                    Name = "Вікно",
                    Description = "Купівля вікон для перших поверхів під'їзду",
                    Amount = 12,
                    MeasurementID = 1,
                    Price = 150.0m,
                    DatePurchase = new DateTime(2016,5,14),
                    IsDone = false,
                    OOABID = 1
                }
            };

            costs.ForEach(item => context.Costs.Add(item));
            context.SaveChanges();

            List<House> Houses = new List<House>
            {
                new House {
                    QuantityFloor = 5,
                    QuantityFlat = 45,
                    QuantityPorch = 3,
                    Street = "вул. Хмельницького",
                    HouseNumber = "4-a",
                    OOABID = 1
                }
            };

            Houses.ForEach(item => context.Houses.Add(item));
            context.SaveChanges();

            List<StateRoom> State_rooms = new List<StateRoom>
            {
                new StateRoom {
                    State = "Житлове"
                },
                new StateRoom {
                    State = "Нежитлове"
                }
            };

            State_rooms.ForEach(item => context.StateRooms.Add(item));
            context.SaveChanges();

            List<TypeOwner> Type_owners = new List<TypeOwner>
            {
                new TypeOwner {
                    Name = "співвласник"
                },
                new TypeOwner {
                    Name = "голова"
                },
                new TypeOwner {
                    Name = "відповідальний по сектору Енергопостачання"
                },
                new TypeOwner {
                    Name = "відповідальний по сектору Прибирання"
                }
            };

            Type_owners.ForEach(item => context.TypeOwners.Add(item));
            context.SaveChanges();

            List<Room> Rooms = new List<Room>
            {
                new Room {
                    HouseID = 1,
                    StateRoomID = 1,
                    Name = "кв. 1",
                    Area = 56.0f
                },

                new Room {
                    HouseID = 1,
                    StateRoomID = 2,
                    Name = "Цокольне приміщення",
                    Area = 15.0f
                },

                new Room {
                    HouseID = 1,
                    StateRoomID = 1,
                    Name = "кв. 2",
                    Area = 38.0f
                }
            };

            Rooms.ForEach(item => context.Rooms.Add(item));
            context.SaveChanges();

            List<Owner> Owners = new List<Owner>
            {
                new Owner {
                    Login = "AvdeevOA",
                    Surname = "Авдеєв",
                    Name = "Олексій",
                    MiddleName = "Аркадійович",
                    State = true,
                    TypeOwnerID = 2,
                    DateRegistration = new DateTime(2016,5,1),
                    Password = "AvdeevOA",
                    Email = "AvdeevOA@gmail.com",
                    OOABID = 1
                },

                new Owner {
                    Login = "BorisovMP",
                    Surname = "Борисов",
                    Name = "Микола",
                    MiddleName = "Петрович",
                    State = true,
                    TypeOwnerID = 3,
                    DateRegistration = new DateTime(2016,5,1),
                    Password = "BorisovMP",
                    Email = "BorisovMP@gmail.com",
                    OOABID = 1
                },

                new Owner {
                    Login = "LitvinovaSP",
                    Surname = "Літвинова",
                    Name = "Серафіма",
                    MiddleName = "Петрівна",
                    State = true,
                    TypeOwnerID = 4,
                    DateRegistration = new DateTime(2016,5,1),
                    Password = "LitvinovaSP",
                    Email = "LitvinovaSP@gmail.com",
                    OOABID = 1
                }
            };

            Owners.ForEach(item => context.Owners.Add(item));
            context.SaveChanges();

            List<OwnerRoom> Owner_rooms = new List<OwnerRoom>
            {
                new OwnerRoom {
                    RoomID = 1,
                    OwnerID = 1,
                    StartDate = new DateTime(2016,5,16)
                },

                new OwnerRoom {
                    RoomID = 2,
                    OwnerID = 2,
                    StartDate = new DateTime(2016,5,16)
                },

                new OwnerRoom {
                    RoomID = 2,
                    OwnerID = 3,
                    StartDate = new DateTime(2016,5,16)
                },

                new OwnerRoom {
                    RoomID = 3,
                    OwnerID = 3,
                    StartDate = new DateTime(2016,5,16)
                },
            };
            Owner_rooms.ForEach(item => context.OwnerRooms.Add(item));
            context.SaveChanges();

            List<Receipt> Receipts = new List<Receipt>
            {
                new Receipt {
                    DatePayment = new DateTime(2016,5,13),
                    Payment = 50.0m,
                    RoomID = 1
                },

                new Receipt {
                    DatePayment = new DateTime(2016,5,14),
                    Payment = 20.0m,
                    RoomID = 2
                }
            };
            Receipts.ForEach(item => context.Receipts.Add(item));
            context.SaveChanges();

            List<Phone> Phones = new List<Phone>
            {
                new Phone {
                    OwnerID = 1,
                    PhoneNumber = "093-567-44-34"
                },

                new Phone {
                    OwnerID = 1,
                    PhoneNumber = "067-567-44-34"
                },
                
                new Phone {
                    OwnerID = 2,
                    PhoneNumber = "073-344-51-84"
                }
            };
            Phones.ForEach(item => context.Phones.Add(item));
            context.SaveChanges();

            List<Message> Messages = new List<Message>
            {
                new Message {
                    OwnerID = 1,
                    Topic = "Збори співвласників",
                    Text = "Збори співвласників для обговорення питань "+ 
                            "витрати коштів відбудуться 25 травня 2016 року. "+
                            "Явка обов'язкова!!!",
                    DatePublication = new DateTime(2016,5,14)
                }
            };
            Messages.ForEach(item => context.Messages.Add(item));
            context.SaveChanges();

            List<Comment> Comments = new List<Comment>
            {
                new Comment {
                    Text = "О котрій годині і де?",
                    DateAdding = new DateTime(2016,5,15),
                    OwnerID = 2,
                    MessageID = 1
                }
            };
            Comments.ForEach(item => context.Comments.Add(item));
            context.SaveChanges();

            List<Interview> Interviews = new List<Interview>
            {
                new Interview {
                    DatePublication = new DateTime(2016,5,14),
                    EndDate = new DateTime(2016,6,14),
                    Topic = "Потужність ламп",
                    Text = "Якою потужністю краще "+
                           "придбати лампи для під'їздів?", 
                    OwnerID = 2
                },
                    new Interview {
                    DatePublication = new DateTime(2016,5,14),
                    EndDate = new DateTime(2016,4,14),
                    Topic = "Колір лавочок",
                    Text = "В який колір пофарбувати"+
                           "лавочки біля під'їздів?", 
                    OwnerID = 2
                    }
            };
            Interviews.ForEach(item => context.Interviews.Add(item));
            context.SaveChanges();

            List<Option> Options = new List<Option>
            {
                new Option {
                    InterviewID = 1,
                    Text = "40 Вт"
                },
                new Option {
                    InterviewID = 1,
                    Text = "60 Вт"
                },
                new Option {
                    InterviewID = 1,
                    Text = "100 Вт"
                },
                new Option {
                    InterviewID = 2,
                    Text = "Жовто-синій"
                },
                new Option {
                    InterviewID = 2,
                    Text = "Коричневий"
                }
            };
            Options.ForEach(item => context.Options.Add(item));
            context.SaveChanges();

            List<Result> Results = new List<Result>
            {
                new Result {
                    OptionID = 1,
                    InterviewID = 1,
                    OwnerID = 1
                },
                new Result {
                    OptionID = 3,
                    InterviewID = 1,
                    OwnerID = 2
                }
            };
            Results.ForEach(item => context.Results.Add(item));
            context.SaveChanges();
        }
    }
}