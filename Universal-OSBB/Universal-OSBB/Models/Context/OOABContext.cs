﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Universal_OSBB.Models.Entities;

namespace Universal_OSBB.Models.Context
{
    public class OOABContext : DbContext
    {
        public DbSet<OOAB> OOABs { get; set; }
        public DbSet<Measurement> Measurements { get; set; }
        public DbSet<Cost> Costs { get; set; }
        public DbSet<House> Houses { get; set; }
        public DbSet<StateRoom> StateRooms { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<TypeOwner> TypeOwners { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<Receipt> Receipts { get; set; }
        public DbSet<OwnerRoom> OwnerRooms { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Interview> Interviews { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<Result> Results { get; set; }

        protected override void OnModelCreating(DbModelBuilder model_builder)
        {
            model_builder.Entity<Comment>()
            .HasRequired(o => o.Owner)
            .WithMany().HasForeignKey(id => id.OwnerID)
            .WillCascadeOnDelete(false);

            model_builder.Entity<Result>()
            .HasRequired(o => o.Owner)
            .WithMany().HasForeignKey(id => id.OwnerID)
            .WillCascadeOnDelete(false);

            model_builder.Entity<Option>()
            .HasRequired(i => i.Interview)
            .WithMany().HasForeignKey(id => id.InterviewID)
            .WillCascadeOnDelete(false);

            model_builder.Entity<House>()
            .HasRequired(i => i.OOAB)
            .WithMany().HasForeignKey(id => id.OOABID)
            .WillCascadeOnDelete(false);
        }

    }
}