﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Universal_OSBB.Models.Entities
{
    public class Phone
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int PhoneID { get; set; }

        [Required]
        public int OwnerID { get; set; }

        public virtual Owner Owner { get; set; }

        [Required]
        public string PhoneNumber { get; set; }
    }
}