﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Universal_OSBB.Models.Entities
{
    public class Cost
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int CostID { get; set; }

        [Required(ErrorMessage = "Введіть найменування закупівлі!")]
        [Display(Name = "Найменування")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Введіть опис закупівлі!")]
        [Display(Name = "Опис")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Введіть кількість!")]
        [Display(Name = "Кількість")]
        [Range(typeof(int), "1", "5000", ErrorMessage = "Некоректне значення кількості!")]
        public int Amount { get; set; }

        [Required]
        public int MeasurementID { get; set; }

        [Display(Name = "Одиниця виміру")]
        public Measurement Measurement { get; set; }

        [Required(ErrorMessage = "Введіть ціну!")]
        [Display(Name = "Ціна, грн")]
        [Range(typeof(decimal), "0,01", "100000,00", ErrorMessage = "Некоректне значення ціни!")]
        public decimal Price { get; set; }

        [NotMapped]
        [Display(Name = "Вартість, грн")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal Payment
        {
            get
            {
                return Amount*Price;
            }
        }

        [Required(ErrorMessage = "Введіть заплановану дату придбання!")]
        [DataType(DataType.Date)]
        [Display(Name = "Запланована дата придбання")]
        public DateTime DatePurchase { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Кінцева дата придбання")]
        public DateTime? DateEnd { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Дата придбання")]
        public DateTime? DateFactual { get; set; }

        [NotMapped]
        [Display(Name = "Виконання")]
        public bool IsDone { get; set; }
        //{
        //    get
        //    {
        //        return DateFactual != null;
        //    }
        //}

        [Required]
        public int OOABID { get; set; }

        public virtual OOAB OOAB { get; set; }
    }
}