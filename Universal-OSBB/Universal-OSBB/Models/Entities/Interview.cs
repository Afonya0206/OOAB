﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Universal_OSBB.Models.Entities
{
    public class Interview
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int InterviewID { get; set; }

        [Required]
        [Display(Name = "Опубліковано")]
        [DataType(DataType.Date)]
        public DateTime DatePublication { get; set; }

        [Required(ErrorMessage = "Введіть тему опитування!")]
        [Display(Name = "Тема")]
        public string Topic { get; set; }

        [Required(ErrorMessage = "Введіть питання!")]
        [Display(Name = "Питання")]
        public string Text { get; set; }


        [Required(ErrorMessage = "Введіть дату завершення опитування!")]
        [Display(Name = "Завершення")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        [Required]
        public int OwnerID { get; set; }

        public virtual Owner Owner { get; set; }

        public virtual ICollection<Option> Options { get; set; }
    }
}