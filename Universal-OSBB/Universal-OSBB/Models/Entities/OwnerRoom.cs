﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Universal_OSBB.Models.Entities
{
    public class OwnerRoom
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int OwnerRoomID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int RoomID { get; set; }

        public virtual Room Room { get; set; }
 
        [HiddenInput(DisplayValue = false)]
        public int OwnerID { get; set; }

        public virtual Owner Owner { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }
    }
}