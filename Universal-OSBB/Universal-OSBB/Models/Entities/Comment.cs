﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Universal_OSBB.Models.Entities
{
    public class Comment
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int CommentID { get; set; }

        [Required]
        [Display(Name="Текст комментария")]
        public string Text { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime DateAdding { get; set; }

        [Required]
        public int OwnerID { get; set; }
        
        public virtual Owner Owner { get; set; }

        [Required]
        public int MessageID { get; set; }

        public virtual Message Message { get; set; }
    }
}