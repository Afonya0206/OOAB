﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Universal_OSBB.Models.Entities
{
    public class Owner
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int OwnerID { get; set; }

        [Required(ErrorMessage = "Введіть прізвище!")]
        [Display(Name="Прізвище")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Введіть им'я!")]
        [Display(Name = "Ім'я")]
        public string Name { get; set; }

        [Display(Name = "По-батькові")]
        public string MiddleName { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                if (MiddleName != null)
                    return Surname + " " + Name + " " + MiddleName;
                return Surname + " " + Name;
            }
        }

        [Required]
        [Display(Name="Дійсний")]
        public bool State { get; set; }

        [Required]
        public int TypeOwnerID { get; set; }

        [Display(Name = "Посада")]
        public virtual TypeOwner TypeOwner { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Дата реєстрації")]
        public DateTime DateRegistration { get; set; }

        [Display(Name = "Логін")]
        public string Login { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Електронна пошта")]
        public string Email { get; set; }

        [Required]
        public int OOABID { get; set; }

        public virtual OOAB OOAB { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Result> Results { get; set; }

        public virtual ICollection<Phone> Phones { get; set; }
    }
}