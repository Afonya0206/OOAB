﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Universal_OSBB.Models.Entities.Registration
{
    public class RegisterUser
    {
        [Required(ErrorMessage = "Введіть прізвище!")]
        [Display(Name = "Прізвище")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Введіть им'я!")]
        [Display(Name = "Ім'я")]
        public string Name { get; set; }

        [Display(Name = "По-батькові")]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Введіть логін!")]
        [Display(Name = "Логін")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Введіть адресу електронної пошти!")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
            ErrorMessage = "Введіть дійсну електронну адресу!")]
        [Display(Name = "Електронна пошта")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Посада")]
        public int TypeOwnerID { get; set; }
    }
}