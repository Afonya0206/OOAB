﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Universal_OSBB.Models.Entities
{
    public class Message
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int MessageID { get; set; }

        [Required] 
        public int OwnerID { get; set; }

        public virtual Owner Owner { get; set; }

        [Required(ErrorMessage = "Введіть тему повідомлення!")]
        [Display(Name = "Тема")]
        public string Topic { get; set; }

        [Required(ErrorMessage = "Введіть текст повідомлення!")]
        [Display(Name = "Текст повідомлення")]
        public string Text { get; set; }

        [Required]
        [Display(Name = "Опубліковано")]
        [DataType(DataType.Date)]
        public DateTime DatePublication { get; set; }
    }
}