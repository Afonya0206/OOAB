﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Universal_OSBB.Models.Entities
{
    public class House
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int HouseID { get; set; }

        [Required(ErrorMessage = "Введіть кількість поверхів!")]
        [Range(1, 50, ErrorMessage = "Некоректне значення кількості поверхів!")]
        [Display(Name = "Кількість поверхів")]
        public int QuantityFloor { get; set; }

        [Required(ErrorMessage = "Введіть кількість квартир!")]
        [Display(Name = "Кількість квартир")]
        [Range(1, 1000, ErrorMessage = "Некоректне значення кількості квартир!")]
        public int QuantityFlat { get; set; }

        [Required(ErrorMessage = "Введіть кількість під'їздів!")]
        [Display(Name = "Кількість під'їздів")]
        [Range(1, 30, ErrorMessage = "Некоректне значення кількості під'їздів!")]
        public int QuantityPorch { get; set; }

        [Required(ErrorMessage = "Введіть вулицю!")]
        [Display(Name = "Вулиця")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Введіть номер будинку!")]
        [Display(Name = "Номер")]
        public string HouseNumber { get; set; }

        [Display(Name = "Будинок")]
        [NotMapped]
        public string Name
        {
            get
            {
                return Street + ", " + HouseNumber;
            }
        }

        public OOAB OOAB { get; set; }

        [Required]
        public int OOABID { get; set; }
    }
}