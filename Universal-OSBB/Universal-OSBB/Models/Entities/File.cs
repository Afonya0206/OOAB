﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Universal_OSBB.Models.Entities
{
    public class File
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int FileID { get; set; }

        [Required]
        [Display(Name = "Назва")]
        public string Name { get; set; }

        [Required]
        public int MessageID { get; set; }

        public virtual Message Message { get; set; }
    }
}