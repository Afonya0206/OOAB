﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Universal_OSBB.Models.Entities
{
    public class Room
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int RoomID { get; set; }

        [Required]
        public int HouseID { get; set; }

        [Display(Name = "Будинок")]
        public virtual House House { get; set; }

        [Required]
        public int StateRoomID { get; set; }

        [Display(Name = "Тип приміщення")]
        public virtual StateRoom StateRoom { get; set; }

        [Required(ErrorMessage = "Введіть назву приміщення!")]
        [Display(Name = "Назва приміщення")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Введіть площу приміщення!")]
        [Display(Name = "Площа")]
        [RegularExpression(@"[0-9,]{1,10}",
            ErrorMessage = "Некоректне значення площі приміщення!")]
        [Range(typeof(float), "1", "1000", ErrorMessage = "Некоректне значення площі приміщення!")]
        
        public float Area { get; set; }

        [NotMapped]
        [Display(Name = "Борг, грн")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal Dept { get; set; }
    }
}