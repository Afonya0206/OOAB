﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Universal_OSBB.Models.Entities.Reports
{
    public class SpentMoneyReport
    {
        [Required(ErrorMessage = "Оберіть початок звітного періоду!")]
        [DataType(DataType.Date)]
        [Display(Name = "з")]
        public DateTime DateFrom { get; set; }

        [Required(ErrorMessage = "Оберіть кінець звітного періоду!")]
        [DataType(DataType.Date)]
        [Display(Name = "до")]
        public DateTime DateTo { get; set; }
    }
}