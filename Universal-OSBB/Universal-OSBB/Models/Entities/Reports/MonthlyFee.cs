﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Universal_OSBB.Models.Entities.Reports
{
    public class MonthlyFee
    {
        [Display(Name = "Місяць")]
        public string NameMonth
        {
            get
            {
                return new DateTime(2016, Month, 1).ToString("MMMM", new CultureInfo("uk-UA"));
            } 
        }

        [Display(Name = "Номер місяця")]
        public int Month { get; set; }

        [Display(Name = "Рік")]
        public int Year { get; set; }

        [Display(Name = "Абонплата")]
        public decimal Payment { get; set; }
    }
}