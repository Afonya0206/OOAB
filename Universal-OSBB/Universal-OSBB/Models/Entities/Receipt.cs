﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Universal_OSBB.Models.Entities
{
    public class Receipt
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ReceiptID { get; set; }

        [Required(ErrorMessage="Оберіть дату сплати коштів!")]
        [Display(Name = "Дата сплати")]
        [DataType(DataType.Date)]
        public DateTime DatePayment { get; set; }

        [Required(ErrorMessage = "Введіть суму сплати коштів!")]
        [Display(Name = "Сума сплати")]
        [Range(typeof(decimal), "1", "10000", ErrorMessage = "Некоректне значення суми сплати!")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal Payment { get; set; }

        [Required(ErrorMessage = "Оберіть приміщення!")]
        public int RoomID { get; set; }

        public virtual Room Room { get; set; }
    }
}