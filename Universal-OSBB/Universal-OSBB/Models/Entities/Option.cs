﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Universal_OSBB.Models.Entities
{
    public class Option
    {
        [Key]
        [Column(Order = 1)] 
        [HiddenInput(DisplayValue = false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int OptionID { get; set; }

        [Key]
        [Column(Order = 2)]
        [HiddenInput(DisplayValue = false)]
        public int InterviewID { get; set; }

        public virtual Interview Interview { get; set; }

        [Required]
        public string Text { get; set; }
    }
}