﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Universal_OSBB.Models.Context;
using Universal_OSBB.Models.Entities;
using System.Data.Entity;

namespace Universal_OSBB.Models.Repository
{
    public class NewsHelper
    {
        static OOABContext db = new OOABContext();

        public static List<Message> GetAllNews(int id_osbb)
        {
            return db.Messages.Include(o => o.Owner).Where(o => o.Owner.OOABID == id_osbb).OrderByDescending(d => d.DatePublication).ToList<Message>();
        }

        public static List<Message> GetLastFiveNews(int id_osbb)
        {
            return db.Messages.Include(o => o.Owner).Where(o => o.Owner.OOABID == id_osbb).OrderByDescending(d => d.DatePublication).Take(5).ToList<Message>();
        }

        public static Message GetNoveltyByID(int identificator)
        {
            return db.Messages.Include(o => o.Owner).Where(id => id.MessageID == identificator).First();
        }

        public static List<File> GetFilesByMessageID(int identificator)
        {
            return db.Files.Where(id => id.MessageID == identificator).ToList<File>();
        }

        public static List<Comment> GetCommentsByMessageID(int identificator)
        {
            return db.Comments.Include(o => o.Owner).Where(id => id.MessageID == identificator)
                   .OrderByDescending(d => d.DateAdding).ToList<Comment>();
        }
    }
}