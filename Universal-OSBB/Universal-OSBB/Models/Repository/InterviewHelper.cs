﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Universal_OSBB.Models.Context;
using Universal_OSBB.Models.Entities;
using System.Data.Entity;

namespace Universal_OSBB.Models.Repository
{
    public class InterviewHelper
    {
        static OOABContext db = new OOABContext();
        public static List<Interview> GetAllInterviews(int id_osbb)
        {
            return db.Interviews.Include(o => o.Owner).Where(o => o.Owner.OOABID == id_osbb).OrderByDescending(d => d.DatePublication).ToList<Interview>();
        }
        public static Interview GetInterviewByID(int identificator)
        {
            return db.Interviews.Include(o => o.Owner).Where(id => id.InterviewID == identificator).First();
        }

        public static List<Option> GetOptionsByInterviewID(int identificator)
        {
            return db.Options.Where(id => id.InterviewID == identificator).ToList<Option>();
        }
        public static bool IsVoted(int idinterview, int idowner)
        {
            Result result = db.Results.Where(idntrvw => idntrvw.InterviewID == idinterview && idntrvw.OwnerID == idowner).FirstOrDefault();
            if (result == null)
                return false;
            return true;
        }
    }
}