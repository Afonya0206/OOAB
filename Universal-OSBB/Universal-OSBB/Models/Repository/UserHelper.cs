﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Universal_OSBB.Models.Context;
using Universal_OSBB.Models.Entities;

namespace Universal_OSBB.Models.Repository
{
    public class UserHelper
    {
        public static bool IsAuthorized(HttpContextBase http_context)
        {
            if (http_context.Session["user_id"] != null)
            {
                int id = Convert.ToInt32(http_context.Session["user_id"].ToString());
                return OwnerHelper.GetOwnerById(id) != null;
            }
            return false;
        }

        public static int? IsValidLoginAndPassword(string login, string password)
        {
            string code = Encryption(password);
            int? id = OwnerHelper.GetIdByLoginAndPassword(login, code);
            return id;
        }

        public static string GetNewPassword()
        {
            string password = "";
            Random rand = new Random();
            
            for (int i = 0; i < 6; i++)
            {
                password = password += rand.Next(0, 9);
            }

            return password;
        }

        public static string Encryption(string password)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(password);

            var csp = new MD5CryptoServiceProvider();

            byte[] byteHash = csp.ComputeHash(bytes);

            string code = string.Empty;

            foreach (byte b in byteHash)
                code += string.Format("{0:x2}", b);

            return code;
        }

        public static void SendToEmail(Owner owner, string password)
        {
            // email отправителя
            MailAddress from = new MailAddress("universal.OSBB@gmail.com", "Універсал-ОСББ");
            // email получателя
            MailAddress to = new MailAddress(owner.Email);
            // создание сообщения
            MailMessage mail = new MailMessage(from, to);
            // тема письма
            mail.Subject = "Інформація для входу в систему";
            // текст письма
            mail.Body = string.Format("{0}, дякуємо за реєстрацію в системі Універсал-ОСББ!<br>"+
                "Ваш Логін: {1}<br>Пароль: {2}<br>" +
                "Бажаємо приємного спілкування та ефективних рішень", owner.Name, owner.Login, password);
            mail.IsBodyHtml = true;
            // адрес smtp-сервера, с которого будет отправлено письмо
            SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
            smtp.EnableSsl = true;

            // логин и пароль отправителя
            smtp.Credentials = new System.Net.NetworkCredential("universal.OSBB@gmail.com", "osbbproject");
            
            smtp.Send(mail);
        }

        public static void AddOwner(Owner owner, List<string> phones, int id_osbb, int type_owner)
        {
            string pswd = GetNewPassword();
            SendToEmail(owner, pswd);
            owner.Password = Encryption(pswd);
            owner.DateRegistration = DateTime.Today;
            owner.State = true;
            owner.OOABID = id_osbb;
            owner.TypeOwnerID = type_owner;
            OwnerHelper.AddOwner(owner);
            OwnerHelper.AddPhones(owner.OwnerID, phones);
        }

        public static void ChangePassword(Owner owner)
        {
            OOABContext db = new OOABContext();
            string GeneratedNewPassword = UserHelper.GetNewPassword();
            string EncryptionPassword = UserHelper.Encryption(GeneratedNewPassword);
            owner.Password = EncryptionPassword;
            db.Owners.Where(o => o.OwnerID == owner.OwnerID).FirstOrDefault().Password = EncryptionPassword;
            db.SaveChanges();
            UserHelper.SendToEmailNewPassword(owner.Email, GeneratedNewPassword);
        }

        public static void SendToEmailNewPassword(string mail, string password)
        {
            // email отправителя
            MailAddress from = new MailAddress("universal.OSBB@gmail.com", "Універсал-ОСББ");
            // email получателя
            MailAddress to = new MailAddress(mail);
            // создание сообщения
            MailMessage createmail = new MailMessage(from, to);
            // тема письма
            createmail.Subject = "Інформація для входу в систему";
            // текст письма
            createmail.Body = string.Format("Ваш пароль у системі Універсал-ОСББ було змінено!<br>" +
                "Ваш новий пароль: {0}<br>" +
                "Бажаємо приємного спілкування та ефективних рішень", password);
            createmail.IsBodyHtml = true;
            // адрес smtp-сервера, с которого будет отправлено письмо
            SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
            smtp.EnableSsl = true;

            // логин и пароль отправителя
            smtp.Credentials = new System.Net.NetworkCredential("universal.OSBB@gmail.com", "osbbproject");

            smtp.Send(createmail);
        }
    }
}