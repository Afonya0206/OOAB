﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Universal_OSBB.Models.Context;
using Universal_OSBB.Models.Entities;
using System.Data.Entity;

namespace Universal_OSBB.Models.Repository
{
    public class OwnerHelper
    {
        static OOABContext db = new OOABContext();

        public static Owner GetOwnerById(int identificator)
        {
            return db.Owners.Include(t => t.TypeOwner).Where(id => id.OwnerID == identificator).FirstOrDefault();
        }

        public static List<Owner> GetResponsibleOwners(int id_osbb)
        {
            return db.Owners.Include(t => t.TypeOwner).
                Where(t => t.TypeOwner.Name != "голова" && 
                    t.TypeOwner.Name != "співвласник" && t.State == true && t.OOABID == id_osbb)
                .ToList<Owner>();
        }

        public static List<Owner> SearchOwnersBySurnameName(int id_osbb, string surname, string name)
        {
            return db.Owners.Include(o => o.TypeOwner).Where(o => o.State == true)
                .Where(o => o.OOABID == id_osbb)
                .Where(s => s.Surname.Contains(surname))
                .Where(n => n.Name.Contains(name))
                .OrderBy(o => o.Surname).ToList<Owner>();
        }

        public static List<Owner> SearchOwnersByHouseRoom(int id_room)
        {
            List<OwnerRoom> ownersofroom = db.OwnerRooms.Include(o => o.Owner).Where(id => id.RoomID == id_room).ToList<OwnerRoom>();
            List<Owner> owners = new List<Owner>();
            foreach (OwnerRoom ownerroom in ownersofroom)
            {
                Owner owner = new Owner();
                owner = db.Owners.Include(o => o.TypeOwner).Where(o => o.State == true).
                        Where(id => id.OwnerID == ownerroom.OwnerID).FirstOrDefault();
                owners.Add(owner);
            }
            return owners;
        }

        public static bool IsHead(int id)
        {
            Owner user = GetOwnerById(id);
            if (user != null)
            {
                Owner head = db.Owners.Where(o => o.TypeOwner.Name == "голова" && o.OOABID == user.OOABID).
                    FirstOrDefault();
                if (head != null)
                    return user.OwnerID == head.OwnerID;
            }
            return false;
        }

        public static List<Phone> GetPhonesById(int id)
        {
            return db.Phones.Where(o => o.OwnerID == id).ToList<Phone>();
        }
        public static Owner GetHeadOfOOAB(int id_OOAB)
        {
            return db.Owners.Where(id => id.OOABID == id_OOAB).FirstOrDefault();
        }       

        public static List<Room> GetPropertiesByID(int id_owner)
        {
            List<OwnerRoom> owner_rooms = db.OwnerRooms.Where(o => o.OwnerID == id_owner).ToList<OwnerRoom>();

            List<Room> rooms = new List<Room>();

            var all_rooms = db.Rooms;

            for (int j = 0; j < owner_rooms.Count(); j++)
            {
                 Room temp = new Room();
                 int id = owner_rooms[j].RoomID;
                 temp = all_rooms.Where(t => t.RoomID == id).First();
                 rooms.Add(temp);
            }

            return rooms;
        }

        public static List<TypeOwner> GetTypesOfOwners()
        {
            return db.TypeOwners.ToList<TypeOwner>();
        }

        public static void AddOwner(Owner owner)
        {
            db.Owners.Add(owner);
            db.SaveChanges();
        }

        public static void AddPhones(int id_owner, List<string> phones)
        {
            foreach(string p in phones)
            {
                if (p != "")
                {
                    Phone temp = new Phone();
                    temp.OwnerID = id_owner;
                    temp.PhoneNumber = p;
                    db.Phones.Add(temp);
                }
            }
            
            db.SaveChanges();
        }

        public static bool IsValidLogin(string login)
        {
            return !db.Owners.Any(usr => string.Compare(usr.Login, login) == 0);            
        }

        public static bool IsValidEmail(string email)
        {
            return !db.Owners.Any(usr => string.Compare(usr.Email, email) == 0);
        }

        public static int? GetIdByLoginAndPassword(string login, string password)
        {
            Owner user = db.Owners.FirstOrDefault(usr => usr.Login == login && usr.Password == password
                && usr.State == true);
            if (user != null)
            {
                return user.OwnerID;
            }
            return null;
        }

        public static List<Owner> GetAllActiveOwners(int id_osbb)
        {
            return db.Owners.Include(o => o.TypeOwner).Where(o => o.State == true).
                Where(o => o.OOABID == id_osbb).OrderBy(o => o.Surname).ToList<Owner>();
        }
        public static List<Owner> GetAllOwners(int id_osbb)
        {
            return db.Owners.Include(o => o.TypeOwner).
                Where(o => o.OOABID == id_osbb).OrderBy(o => o.Surname).ToList<Owner>();
        }

        public static void MakeNotActive(int id_owner)
        {
            Owner owner = db.Owners.Where(o => o.OwnerID == id_owner).FirstOrDefault();
            if (owner != null)
            {
                owner.State = false;
                db.SaveChanges();
            }
        }

        public static void MakeActive(int id_owner)
        {
            Owner owner = db.Owners.Where(o => o.OwnerID == id_owner).FirstOrDefault();
            if (owner != null)
            {
                owner.State = true;
                db.SaveChanges();
            }
        }

        public static int AddNewType(string new_type_owner)
        {
            List<TypeOwner> type_owners = db.TypeOwners.ToList<TypeOwner>();
            foreach(TypeOwner t in type_owners)
            {
                if (t.Name == new_type_owner)
                    return t.TypeOwnerID;
            }
            TypeOwner type_owner = new TypeOwner();
            type_owner.Name = new_type_owner;
            db.TypeOwners.Add(type_owner);
            db.SaveChanges();

            type_owner = db.TypeOwners.Where(t => t.Name == new_type_owner).First();
            return type_owner.TypeOwnerID;
        }

        public static int GetIdOSBBByIdOwner(int id_owner)
        {
            Owner owner = db.Owners.Where(o => o.OwnerID == id_owner).FirstOrDefault();
            return owner.OOABID;
        }

        public static OOAB GetOSBBById(int id_osbb)
        {
            OOAB ooab = db.OOABs.Where(id => id.OOABID == id_osbb).FirstOrDefault();
            return ooab;
        }

        public static int? IsPossibleOwner(string surname_name, int id_osbb, int id_room)
        {
            List<Owner> owners = GetPossibleOwners(id_osbb, id_room);
            foreach (Owner owner in owners)
            {
                if (owner.Surname + " " + owner.Name == surname_name)
                    return owner.OwnerID;
            }
            return null;
        }

        public static List<Owner> GetPossibleOwners(int id_osbb, int id_room)
        {
            List<Owner> owners = GetAllActiveOwners(id_osbb);
            List<Owner> possible_owners = new List<Owner>();

            List<OwnerRoom> owner_room = db.OwnerRooms.Where(o => o.RoomID == id_room).ToList<OwnerRoom>();

            foreach(Owner item in owners)
            {
                if(!owner_room.Any(o => o.OwnerID == item.OwnerID))
                    possible_owners.Add(item);
            }

            return possible_owners;
        }

        public static void UpdateChangeOwner(Owner update_owner)
        {
            Owner owner = db.Owners.Where(o => o.OwnerID == update_owner.OwnerID).FirstOrDefault();
            owner.Surname = update_owner.Surname;
            owner.Name = update_owner.Name;
            owner.MiddleName = update_owner.MiddleName;
            owner.State = update_owner.State;
            owner.TypeOwnerID = update_owner.TypeOwnerID;
            db.SaveChanges();
        }

        public static Owner GetOwnerByEmail(string mail)
        {
            return db.Owners.Where(email => String.Compare(email.Email, mail) == 0).FirstOrDefault();
        }
    }
}