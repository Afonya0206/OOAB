﻿using Novacode;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace Universal_OSBB.Models.Repository
{
    public class WordHelper
    {
        public static byte[] CreateStatementToHead(string NameOSBB, string FullNameHead, string FullNameOwner, string MainTextStatement, DateTime DateNow)
        {
            MemoryStream stream = new MemoryStream();
            DocX doc = DocX.Create(stream);

            Paragraph ParHead = doc.InsertParagraph();
            ParHead.Append("Голові ОСББ \"" + NameOSBB + "\"").Font(new FontFamily("Times New Roman")).FontSize(14).Alignment = Alignment.right;
            ParHead.AppendLine(FullNameHead).Font(new FontFamily("Times New Roman")).FontSize(14);
            ParHead.AppendLine("члена ОСББ").Font(new FontFamily("Times New Roman")).FontSize(14);
            ParHead.AppendLine(FullNameOwner).Font(new FontFamily("Times New Roman")).FontSize(14);

            doc.InsertParagraph().Font(new FontFamily("Times New Roman")).FontSize(14);
            doc.InsertParagraph().Font(new FontFamily("Times New Roman")).FontSize(14);
            Paragraph ParSatement = doc.InsertParagraph().Font(new FontFamily("Times New Roman")).FontSize(14);
            ParSatement.Append("Заява").Font(new FontFamily("Times New Roman")).FontSize(14).Alignment = Alignment.center;
            doc.InsertParagraph().Font(new FontFamily("Times New Roman")).FontSize(14);

            Paragraph ParMain = doc.InsertParagraph();
            ParMain.IndentationFirstLine = 1;
            ParMain.Append("Я, " + FullNameOwner + ", " + MainTextStatement).Font(new FontFamily("Times New Roman")).FontSize(14).Alignment = Alignment.both;
            Paragraph ParFooter = doc.InsertParagraph().Font(new FontFamily("Times New Roman")).FontSize(14);
            Table table = doc.AddTable(1, 2);
            table.Rows[0].Cells[0].Paragraphs.First().Append(DateNow.ToShortDateString() + "р.").Font(new FontFamily("Times New Roman")).FontSize(14).Alignment = Alignment.left;
            table.Rows[0].Cells[1].Paragraphs.First().Append("________________").Font(new FontFamily("Times New Roman")).FontSize(14).Alignment = Alignment.center;
            table.Rows[0].Cells[1].Paragraphs.First().AppendLine("(підпис)").Font(new FontFamily("Times New Roman")).FontSize(14).Alignment = Alignment.center;
            ParFooter.InsertTableAfterSelf(table);
            table.Rows[0].Cells[0].Width = Convert.ToDouble(1200);
            table.Rows[0].Cells[1].Width = Convert.ToDouble(40);
            ParFooter.IndentationFirstLine = 1;

            doc.Save();

            return stream.ToArray();
        }
    }
}