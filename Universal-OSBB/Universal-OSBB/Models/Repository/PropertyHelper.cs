﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Universal_OSBB.Models.Context;
using Universal_OSBB.Models.Entities;
using System.Data.Entity;

namespace Universal_OSBB.Models.Repository
{
    public class PropertyHelper
    {
        static OOABContext db = new OOABContext();

        public static List<Room> GetAllRooms(int osbb_id)
        {
            return db.Rooms.Include(r => r.House).Include(r => r.StateRoom).
                Where(r => r.House.OOABID == osbb_id).ToList<Room>();
        }

        public static Room GetRoomById(int id_room)
        {
            return db.Rooms.Include(r => r.House).Include(r => r.StateRoom).Where(r => r.RoomID == id_room).FirstOrDefault();
        }

        public static List<Owner> GetOwnersById(int id_room)
        {
            List<OwnerRoom> owner_rooms = db.OwnerRooms.Where(o => o.RoomID == id_room).ToList<OwnerRoom>();

            List<Owner> owners = new List<Owner>();

            var all_owners = db.Owners.Where(o => o.State == true);

            for (int j = 0; j < owner_rooms.Count(); j++)
            {
                Owner temp = new Owner();
                int id = owner_rooms[j].OwnerID;
                temp = all_owners.Where(t => t.OwnerID == id).FirstOrDefault();
                if(temp != null)
                    owners.Add(temp);
            }

            return owners;
        }
        public static void AddOwnerToRoom(int id_room, int id_owner)
        {
            OwnerRoom owner_room = new OwnerRoom();
            owner_room.OwnerID = id_owner;
            owner_room.RoomID = id_room;
            owner_room.StartDate = DateTime.Now;
            db.OwnerRooms.Add(owner_room);
            db.SaveChanges();
        }
        public static void DeleteOwner(int id_room, int id_owner)
        {
            OwnerRoom owner_room = db.OwnerRooms.Where(t => t.RoomID == id_room && t.OwnerID == id_owner).
                FirstOrDefault();

            db.OwnerRooms.Remove(owner_room);
            db.SaveChanges();        
        }
        public static void AddHouse(House house)
        {
            db.Houses.Add(house);
            db.SaveChanges();
        }
        public static void AddRoom(Room room)
        {
            db.Rooms.Add(room);
            db.SaveChanges();
        }  

        public static List<House> GetHousesByIdOSBB(int id_osbb)
        {
            List<House> houses = db.Houses.Where(h => h.OOABID == id_osbb).ToList<House>();
            return houses;
        }

        public static List<StateRoom> GetRoomStates()
        {
            List<StateRoom> states = db.StateRooms.ToList<StateRoom>();
            return states;
        }

        public static List<Room> GetRoomsByIdHouse(int id_house)
        {
            List<Room> rooms = db.Rooms.Where(r => r.HouseID == id_house).ToList<Room>();
            return rooms;
        }

        public static float GetAllAreaRooms(int id_osbb, int month, int year)
        {
            return db.Rooms.Where(o => o.House.OOABID == id_osbb).Sum(o => o.Area);
        }

        public static int GetIdOSBBByIdRoom(int id_room)
        {
            return db.Rooms.Include(r => r.House).Where(r => r.RoomID == id_room).FirstOrDefault().House.OOABID;
        }
    }   
}