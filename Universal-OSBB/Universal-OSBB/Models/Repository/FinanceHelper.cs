﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Universal_OSBB.Models.Context;
using Universal_OSBB.Models.Entities;
using System.Data.Entity;
using Universal_OSBB.Models.Entities.Reports;
using System.Globalization;

namespace Universal_OSBB.Models.Repository
{
    public class FinanceHelper
    {
        static OOABContext db = new OOABContext();

        public static List<Measurement> GetMeasurements()
        {
            return db.Measurements.ToList<Measurement>();
        }

        public static void AddPurchasedProduct(Cost product)
        {
            db.Costs.Add(product);
            db.SaveChanges();
        }

        public static List<Receipt> GetReceiptsForRooms(int id_osbb, int id_owner)
        {
            List<OwnerRoom> roomsowner = db.OwnerRooms.Include(o => o.Owner).
                                         Include(r => r.Room).
                                         Where(id => id.OwnerID == id_owner).ToList<OwnerRoom>();
            List<Receipt> receipts = new List<Receipt>();
            foreach (OwnerRoom ownerroom in roomsowner)
            {
                List<Receipt> tempreceipts = new List<Receipt>();
                tempreceipts = db.Receipts.Where(id => id.RoomID == ownerroom.RoomID).ToList<Receipt>();
                if (tempreceipts.Count() == 0)
                {
                    Receipt temp = new Receipt();
                    Room room = db.Rooms.Where(id => id.RoomID == ownerroom.RoomID).FirstOrDefault();
                    temp.Room = room;
                    temp.RoomID = room.RoomID;
                    receipts.Add(temp);
                }
                else
                    foreach (Receipt tempreceipt in tempreceipts)
                    {
                        receipts.Add(tempreceipt);
                    }
            }
            return receipts;
        }


        public static decimal TotalPaymentByIdRoom(int id_room, int id_osbb, int id_owner)
        {
            List<Receipt> receipts = GetReceiptsForRooms(id_osbb, id_owner).Where(id => id.RoomID == id_room).ToList<Receipt>();
            decimal TotalForRoom = 0;
            for (int i = 0; i < receipts.Count(); i++)
                TotalForRoom += receipts[i].Payment;
            return TotalForRoom;
        }

        public static List<Cost> GetSpentMoney(int id_osbb, DateTime? from, DateTime? to)
        {
            List<Cost> products = db.Costs.Include(p => p.Measurement).Where(p => p.OOABID == id_osbb && p.DateFactual != null).
                OrderBy(p => p.DatePurchase).ToList<Cost>();
            if(from == null || to == null || from > to)
                return products;
            return products.Where(p => p.DateFactual.Value.Date >= from && p.DateFactual.Value.Date <= to).ToList<Cost>(); ;
        }

        public static decimal TotalSpentMoney(List<Cost> products)
        {
            decimal sum = products.Select(p => new
                   {
                        OOABID = p.OOABID,
                        Payment = p.Amount*p.Price,
                        Date_purchase = p.DatePurchase
                   }).Sum(p => p.Payment);
            return sum;
        }

        public static MonthlyFee MonthlyFee(int id_osbb, int month, int year)
        {
            MonthlyFee mf = new MonthlyFee();

            List<Cost> purchases = db.Costs.
                Where(p => p.OOABID == id_osbb).ToList<Cost>();

            float area_rooms = PropertyHelper.GetAllAreaRooms(id_osbb, month, year);

            mf.Month = month;
            mf.Year = year;

            List<Cost> one_time_purchases = purchases.
                                Where(p => (p.DateEnd == null && p.DatePurchase.Month == month && p.DatePurchase.Year == year) || // если покупка единоразовая
                (p.DatePurchase.Month == month && p.DatePurchase.Year == year &&
                (int)Math.Ceiling((((DateTime)p.DateEnd - p.DatePurchase).Days / 31.0)) == 0)).      // если покупка в течение одного месяца
                ToList<Cost>();

            decimal sum = one_time_purchases.Select(p => new
            {
                Payment = p.Amount * p.Price,
            }).Sum(p => p.Payment);


            DateTime date = new DateTime(year, month, 1);

            List<Cost> durable_purchases = purchases.
                Where(p => p.DateEnd != null && date >= new DateTime(p.DatePurchase.Year, p.DatePurchase.Month, 1) &&
                date <= new DateTime(((DateTime)p.DateEnd).Year, ((DateTime)p.DateEnd).Month, DateTime.DaysInMonth(((DateTime)p.DateEnd).Year,
                    ((DateTime)p.DateEnd).Month)) &&
                    ((int)Math.Ceiling((((DateTime)p.DateEnd - p.DatePurchase).Days / 31.0)) != 0)).ToList<Cost>();


            sum += durable_purchases.Select(p => new
            {
                Payment = p.Amount * p.Price / (int)Math.Ceiling((((DateTime)p.DateEnd - p.DatePurchase).Days/31.0))
            }).Sum(p => p.Payment);

            mf.Payment = sum / (decimal)area_rooms;

            return mf;
        }

        public static List<MonthlyFee> MonthlyFeesBy12(int id_osbb, int month_from, int year_from)
        {
            List<MonthlyFee> monthly_fees = new List<MonthlyFee>();

            List<Cost> planned_purchase = db.Costs.ToList<Cost>();

            int year = year_from;
            int month = month_from;

            float area_rooms = PropertyHelper.GetAllAreaRooms(id_osbb, month_from, year_from);

            for (int i = 0; i < 12; i++)
            {
                if (month == 13)
                {
                    month = 1;
                    year = year_from + 1;
                }

                MonthlyFee temp = MonthlyFee(id_osbb, month, year);

                monthly_fees.Add(temp);

                month++;
            }


            return monthly_fees; 
        }

        public static void AddReceipt(Receipt receipt)
        {
            db.Receipts.Add(receipt);
            db.SaveChanges();
        }

        public static List<Cost> GetFutureCostPlanning(int id_osbb)
        {
            DateTime now =  DateTime.Now;
            DateTime next_month;
            if (now.Month == 12)
                next_month = new DateTime(now.Year + 1, 1, 1);
            else
                next_month = new DateTime(now.Year, now.Month + 1, 1);

            List<Cost> costs = db.Costs.
                Where(c => c.OOABID == id_osbb && c.DatePurchase  >= next_month).OrderBy( c => c.DatePurchase).
                ToList<Cost>();

            return costs;
        }

        public static List<Cost> GetCurrentCostPlanning(int id_osbb)
        {
            DateTime now = DateTime.Now;
            List<Cost> costs = db.Costs.Include(c => c.Measurement).
                Where(c => (c.OOABID == id_osbb && c.DateEnd == null && c.DatePurchase.Month == now.Month && c.DatePurchase.Year == now.Year) ||
                    (c.OOABID == id_osbb && c.DateEnd != null && now >= c.DatePurchase && now <= c.DateEnd)).
                OrderBy(c => c.DatePurchase).
                ToList<Cost>();

            return costs;
        }

        public static Cost GetProductById(int id_product)
        {
            return db.Costs.Where(p => p.CostID == id_product).FirstOrDefault();
        }

        public static void DeleteProductById(int id_product)
        {
            Cost product = db.Costs.Where(p => p.CostID == id_product).FirstOrDefault();
            db.Costs.Remove(product);
            db.SaveChanges();
        }
        public static void UpdateProduct(Cost product)
        {

            Cost prod = db.Costs.Where(p => p.CostID == product.CostID).FirstOrDefault();
            
            if (prod != null)
            {
                prod.Name = product.Name;
                prod.Description = product.Description;
                prod.Amount = product.Amount;
                prod.MeasurementID = product.MeasurementID;
                prod.Price = product.Price;
                prod.DatePurchase = product.DatePurchase;
                prod.DateEnd = product.DateEnd;

                db.SaveChanges();
            }
        }

        public static void UpdatePerform(List<Cost> update_costs)
        {
            List<Cost> costs = db.Costs.ToList<Cost>();
            foreach (Cost cost in update_costs)
            {
                Cost temp = db.Costs.Where(c => c.CostID == cost.CostID).FirstOrDefault();
                if (temp != null && cost.IsDone && cost.DateFactual == null)
                {
                    temp.DateFactual = DateTime.Now;
                    temp.IsDone = true;
                }

                db.SaveChanges();
            }
        }

        public static decimal DeptOfRoom (int id_room)
        {
            Room room = PropertyHelper.GetRoomById(id_room);
            decimal dept = 0;  
            decimal total_paid_money = 0;
            decimal sum_monthly_fees = 0;
            int id_osbb = PropertyHelper.GetIdOSBBByIdRoom(id_room);

            if(db.Receipts.Any(r => r.RoomID == id_room))
                total_paid_money = db.Receipts.Where(r => r.RoomID == id_room).Sum(r => r.Payment);

            DateTime min_date = db.Costs.Select(c => new
                {
                    date = c.DatePurchase
                }
                ).Min(c => c.date);

            int year = min_date.Year;
            int month = min_date.Month;

            DateTime now = DateTime.Now;

            while(year <= now.Year && month <= now.Month)
            {
                sum_monthly_fees += MonthlyFee(id_osbb, month, year).Payment * (decimal)room.Area;
                if(month == 12)
                {
                    year++;
                    month = 1;
                }
                else
                {
                    month++;
                }
            }

            dept = sum_monthly_fees - total_paid_money;
            return dept;
        }

        public static List<Room> GetDeptors(int id_osbb)
        {
            List<Room> rooms = PropertyHelper.GetAllRooms(id_osbb);
            List<Room> deptors = new List<Room>();
            decimal dept;

            foreach(var room in rooms)
            {
                dept = DeptOfRoom(room.RoomID);
                if (dept > 0)
                {
                    room.Dept = dept;
                    deptors.Add(room);
                }
            }

            return deptors;          
        }        
    }
}