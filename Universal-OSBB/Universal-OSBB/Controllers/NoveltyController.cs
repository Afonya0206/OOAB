﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Universal_OSBB.Models.Context;
using Universal_OSBB.Models.Entities;
using Universal_OSBB.Models.Repository;

namespace Universal_OSBB.Controllers
{
    public class NoveltyController : Controller
    {
        OOABContext db = new OOABContext();

        public ActionResult News()
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            return View(NewsHelper.GetAllNews(id_osbb));
        }

        public ActionResult CreateNovelty()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SubmitCreateNovelty(Message message, HttpPostedFileBase upload)
        {
            message.DatePublication = DateTime.Now;
            message.OwnerID = int.Parse(Session["user_id"].ToString());
            if (ModelState.IsValid)
            {
                db.Messages.Add(message);
                db.SaveChanges();
                Message LastMessage = db.Messages.Where(id => id.MessageID == message.MessageID).First<Message>();
                if (upload != null)
                {
                    File file = new File();
                    file.Name = System.IO.Path.GetFileName(upload.FileName);
                    file.MessageID = LastMessage.MessageID;
                    db.Files.Add(file);
                    db.SaveChanges();
                    file = db.Files.Where(id => id.FileID == file.FileID).First();

                    //виділення розширення файлу
                    int i = file.Name.Length;
                    while (i >= 0 && String.Compare(file.Name[i - 1].ToString(), ".") != 0)
                        i--;

                    file.Name = file.Name.Insert(i - 1, file.FileID.ToString());
                    db.SaveChanges();
                    upload.SaveAs(Server.MapPath("~/FilesForNews/" + file.Name));

                }
                return View("News", NewsHelper.GetAllNews(OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()))));
            }
            else
            {
                return View("CreateNovelty");
            }
            
        }

        public ActionResult DetailsNovelty(int id)
        {
            return View(NewsHelper.GetNoveltyByID(id));
        }

        public ActionResult FilesMessage(int id)
        {
            return PartialView(NewsHelper.GetFilesByMessageID(id));
        }

        public FileResult DownloadFile(string filename)
        {
            // Путь до файлу
            string filepath = Server.MapPath("~/FilesForNews/" + filename);
            string filetype = "";

            if (filename.Contains(".pdf"))
                filetype = "application/pdf";
            else
                if (filename.Contains(".docx") || filename.Contains(".doc"))
                    filetype = "application/doc";

            if (String.Compare(filetype, "") == 0)
                return File(filepath, filename);
            return File(filepath, filetype, filename);
        }

        public ActionResult CommentsNovelty(int id)
        {
            return PartialView(NewsHelper.GetCommentsByMessageID(id));
        }

        public ActionResult AddComment(int id_owner, int id_message, string text_comment)
        {
            Comment comment = new Comment();
            comment.Text = text_comment;
            comment.DateAdding = DateTime.Now;
            comment.OwnerID = id_owner;
            comment.MessageID = id_message;
            db.Comments.Add(comment);
            db.SaveChanges();

            return View("DetailsNovelty", NewsHelper.GetNoveltyByID(id_message));
        }
    }
}