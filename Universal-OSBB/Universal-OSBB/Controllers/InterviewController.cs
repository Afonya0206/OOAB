﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Universal_OSBB.Models.Context;
using Universal_OSBB.Models.Entities;
using Universal_OSBB.Models.Repository;
using System.Data.Entity;

namespace Universal_OSBB.Controllers
{
    public class InterviewController : Controller
    {
        OOABContext db = new OOABContext();
        public ActionResult Interviews()
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            return View(InterviewHelper.GetAllInterviews(id_osbb));
        }
        public ActionResult CreateInterview()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SubmitCreateInterview(Interview interview, List<string> opt)
        {
            if (opt.Contains(""))
                ModelState.AddModelError("EndDate", "Введіть варіанти відповіді!");


            if (ModelState.IsValid)
            {
                if (interview.EndDate <= DateTime.Now)
                {
                    ModelState.AddModelError("EndDate", "Дата завершення опитування повинна бути більшою за поточну!");
                    return View("CreateInterview");
                }
            
                interview.DatePublication = DateTime.Now;
                interview.OwnerID = int.Parse(Session["user_id"].ToString());
                for (int i = 0; i < opt.Count(); i++)
                {
                    Option TempOption = new Option();
                    TempOption.InterviewID = interview.InterviewID;
                    TempOption.Text = opt[i];
                    db.Options.Add(TempOption);
                }
                db.Interviews.Add(interview);
                db.SaveChanges();

                int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
                return View("Interviews", InterviewHelper.GetAllInterviews(id_osbb));                    
            }
            else
                return View("CreateInterview");
        }

        public ActionResult OptionsInterview(int id)
        {
            return PartialView(InterviewHelper.GetOptionsByInterviewID(id));
        }

        public ActionResult DetailsInterview(int id)
        {
            return View(InterviewHelper.GetInterviewByID(id));
        }

        public ActionResult SubmitVote(string RadioButtonOptionID, int InterviewID)
        {
            Result result = new Result();
            result.OptionID = int.Parse(RadioButtonOptionID);
            result.InterviewID = InterviewID;
            result.OwnerID = int.Parse(Session["user_id"].ToString());
            db.Results.Add(result);
            db.SaveChanges();
            return View("DetailsInterview", InterviewHelper.GetInterviewByID(InterviewID));
        }
    }
}
