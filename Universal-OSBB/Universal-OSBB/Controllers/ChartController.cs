﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using Universal_OSBB.Models.Context;
using System.Data.Entity;
using Universal_OSBB.Models.Entities;
using Universal_OSBB.Models.Repository;

namespace Universal_OSBB.Controllers
{
    public class ChartController : Controller
    {
        OOABContext db = new OOABContext();
        List<Tuple<int, string>> DatesForTable = new List<Tuple<int, string>>();
        public FileContentResult GetChart(int identificator)
        {
            var dates = new List<Tuple<float, string>>();
            
            List<Option> options = db.Options.Where(id => id.InterviewID == identificator).ToList<Option>();

            int CountOwnerInOSBB = OwnerHelper.GetAllOwners(OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()))).Count();

            foreach (Option option in options)
            {
                int CountOption = db.Results.Where(id => id.OptionID == option.OptionID && id.InterviewID == identificator).Count();
                if (CountOption > 0)
                {
                    float InPercent = (float)CountOption * 100 / CountOwnerInOSBB;
                    dates.Add(new Tuple<float, string>(InPercent, option.Text));
                }
            }

            dates.Sort();
            dates.Reverse();

            DatesForTable.Sort();
            DatesForTable.Reverse();

            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));

            int CountNotVoteOwners = (from o in db.Owners.Where(id => id.OOABID == id_osbb)
                                      join r in db.Results.Where(id => id.InterviewID == identificator)
                                      on o.OwnerID equals r.OwnerID into or
                                      from r in or.DefaultIfEmpty()
                                      where r == null
                                      select r).Count();

            if (CountNotVoteOwners > 0)
            {
                dates.Add(new Tuple<float, string>((float)CountNotVoteOwners * 100 / CountOwnerInOSBB, "Не голосували"));
            }

            var chart = new Chart();
            chart.Width = 700;
            chart.Height = 300;
            chart.BackColor = Color.FromArgb(211, 223, 240);
            chart.BorderlineDashStyle = ChartDashStyle.Solid;
            chart.BackSecondaryColor = Color.White;
            chart.BackGradientStyle = GradientStyle.TopBottom;
            chart.BorderlineWidth = 1;
            chart.Palette = ChartColorPalette.BrightPastel;
            chart.BorderlineColor = Color.FromArgb(26, 59, 105);
            chart.RenderType = RenderType.BinaryStreaming;
            chart.BorderSkin.SkinStyle = BorderSkinStyle.Emboss;
            chart.AntiAliasing = AntiAliasingStyles.All;
            chart.TextAntiAliasingQuality = TextAntiAliasingQuality.Normal;
            chart.Legends.Add(CreateLegend());
            chart.Series.Add(CreateSeries(dates, SeriesChartType.Pie, Color.Red));
            chart.ChartAreas.Add(CreateChartArea());

            var ms = new MemoryStream();
            chart.SaveImage(ms);
            return File(ms.GetBuffer(), @"image/png");
            
        }

        [NonAction]
        public Series CreateSeries(IList<Tuple<float, string>> results, SeriesChartType chartType, Color color)
        {
            var seriesDetail = new Series();
            seriesDetail.Name = "Result Chart";
            seriesDetail.LabelFormat = "N2";
            seriesDetail.IsValueShownAsLabel = true;
            seriesDetail.Color = color;
            seriesDetail.ChartType = chartType;
            seriesDetail.BorderWidth = 2;
            seriesDetail["DrawingStyle"] = "Cylinder";
            seriesDetail["PieDrawingStyle"] = "SoftEdge";
            DataPoint point;

            foreach (var result in results)
            {
                point = new DataPoint();
                point.AxisLabel = result.Item2;
                point.YValues = new double[] { result.Item1 };
                seriesDetail.Points.Add(point);
            }
            seriesDetail.ChartArea = "Result Chart";

            return seriesDetail;
        }

        [NonAction]
        public Legend CreateLegend()
        {
            var legend = new Legend();
            legend.Docking = Docking.Bottom;
            legend.Alignment = StringAlignment.Center;
            legend.BackColor = Color.Transparent;
            legend.Font = new Font(new FontFamily("Trebuchet MS"), 9);
            legend.LegendStyle = LegendStyle.Column;
            legend.Docking = Docking.Right;

            return legend;
        }

        [NonAction]
        public ChartArea CreateChartArea()
        {
            var chartArea = new ChartArea();
            chartArea.BackColor = Color.Transparent;
            chartArea.Name = "Result Chart";
            chartArea.AxisX.IsLabelAutoFit = false;
            chartArea.AxisY.IsLabelAutoFit = false;
            chartArea.AxisX.LabelStyle.Font = new Font("Verdana,Arial,Helvetica,sans-serif", 8F, FontStyle.Regular);
            chartArea.AxisY.LabelStyle.Font = new Font("Verdana,Arial,Helvetica,sans-serif", 8F, FontStyle.Regular);
            chartArea.AxisY.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisY.MajorGrid.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.MajorGrid.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.Interval = 1;
            return chartArea;
        }

        public ActionResult ShowResultInTable(int identificator)
        {
            List<Option> options = db.Options.Where(id => id.InterviewID == identificator).ToList<Option>();

            int CountOwnerInOSBB = OwnerHelper.GetAllOwners(OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()))).Count();

            foreach (Option option in options)
            {
                int CountOption = db.Results.Where(id => id.OptionID == option.OptionID && id.InterviewID == identificator).Count();
                DatesForTable.Add(new Tuple<int, string>(CountOption, option.Text));
            }

            DatesForTable.Sort();
            DatesForTable.Reverse();

            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));

            int CountNotVoteOwners = (from o in db.Owners.Where(id => id.OOABID == id_osbb)
                                      join r in db.Results.Where(id => id.InterviewID == identificator)
                                      on o.OwnerID equals r.OwnerID into or
                                      from r in or.DefaultIfEmpty()
                                      where r == null
                                      select r).Count();

            if (CountNotVoteOwners > 0)
            {
                DatesForTable.Add(new Tuple<int, string>(CountNotVoteOwners, "Не голосували"));
            }
            return PartialView(DatesForTable);
        }
    }
}
