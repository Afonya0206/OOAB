﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Universal_OSBB.Models.Entities;
using Universal_OSBB.Models.Repository;

namespace Universal_OSBB.Controllers
{
    public class PropertyController : Controller
    {
        public ActionResult Rooms()
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"]));
            return View(PropertyHelper.GetAllRooms(id_osbb));
        }

        public ActionResult CreateRoom()
        {
            FillViewBagLists();
            return View();
        }

        public ActionResult CreateHouse()
        {
            return View();
        }

        public ActionResult Houses()
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            return View(PropertyHelper.GetHousesByIdOSBB(id_osbb));
        }

        public ActionResult DetailsRoom(int id_room)
        {
            ViewBag.Dept = FinanceHelper.DeptOfRoom(id_room);
            return View(PropertyHelper.GetRoomById(id_room));
        }

        public ActionResult OwnersOfRoom(int id_room)
        {
            return PartialView(PropertyHelper.GetOwnersById(id_room));
        }
        public ActionResult AddOwnerToRoom(int id_room)
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));

            ViewBag.Id_room = id_room;
            return PartialView(OwnerHelper.GetPossibleOwners(id_osbb, id_room));
        }

        public ActionResult SubmitOwnerToRoom(string value, int id_room)
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"]));
            int? id_owner = OwnerHelper.IsPossibleOwner(value, id_osbb, id_room);
            if(id_owner == null)
                TempData["Error"] = "Оберіть власника зі списку!";
            else
            {
                PropertyHelper.AddOwnerToRoom(id_room, (int)id_owner);
            }
            return RedirectToAction("DetailsRoom", new { id_room = id_room });
        }
        public ActionResult DeleteOwner(int id_room, int id_owner)
        {
            PropertyHelper.DeleteOwner(id_room, id_owner);

            return RedirectToAction("DetailsRoom", new { id_room = id_room });
        }
        public ActionResult AddHouse(House new_house)
        {
            new_house.OOABID = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            if (ModelState.IsValid)
            {
                PropertyHelper.AddHouse(new_house);
                ViewBag.Result = "Будинок успішно зареєстровано!";
                return View("AdditionSuccessful");
            }
            else
            {
                return View("CreateHouse");
            }
        }

        public ActionResult AddRoom(Room new_room)
        {
            if (ModelState.IsValid)
            {
                PropertyHelper.AddRoom(new_room);
                ViewBag.Result = "Приміщення успішно зареєстровано!";
                return View("AdditionSuccessful");
            }
            else
            {
                FillViewBagLists();
                return View("CreateRoom");
            }
        } 

        public void FillViewBagLists()
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            List<House> houses = PropertyHelper.GetHousesByIdOSBB(id_osbb);
            SelectList list_houses = new SelectList(houses, "HouseID", "Name");
            if (list_houses.Count() > 0)
                list_houses.First().Selected = true;
            ViewBag.Houses = list_houses;

            SelectList states = new SelectList(PropertyHelper.GetRoomStates(), "StateRoomID", "State");
            ViewBag.States = states;
        }
    }
}
