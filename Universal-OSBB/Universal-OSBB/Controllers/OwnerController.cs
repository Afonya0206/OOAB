﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Universal_OSBB.Models;
using Universal_OSBB.Models.Entities;
using Universal_OSBB.Models.Repository;

namespace Universal_OSBB.Controllers
{
    public class OwnerController : Controller
    {
        public ActionResult DetailsOwner(int id)
        {
            return View(OwnerHelper.GetOwnerById(id));
        }

        public ActionResult StatementToHead()
        {
            int id_owner = Convert.ToInt32(Session["user_id"].ToString());
            ViewBag.Owner = OwnerHelper.GetOwnerById(id_owner).FullName;
            return View();
        }

        public ActionResult CreateStatementToHead(string MainTextStatement)
        {
            if (MainTextStatement == "")
                ModelState.AddModelError("MainText", "Введіть текст заяви!");
            if (!ModelState.IsValid)
            {
                int id_owner = Convert.ToInt32(Session["user_id"].ToString());
                ViewBag.Owner = OwnerHelper.GetOwnerById(id_owner).FullName;
                return View("StatementToHead");
            }
                
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(int.Parse(Session["user_id"].ToString()));
            Owner head = OwnerHelper.GetHeadOfOOAB(id_osbb);
            string FullNameHead = head.Surname + " " + head.Name[0] + "." + head.MiddleName[0] + ".";
            OOAB osbb = OwnerHelper.GetOSBBById(id_osbb);
            Owner sender = OwnerHelper.GetOwnerById(int.Parse(Session["user_id"].ToString()));
            string FullNameOwner = sender.Surname + " " + sender.Name + " " + sender.MiddleName;
            return File(WordHelper.CreateStatementToHead(osbb.Name, FullNameHead, FullNameOwner, MainTextStatement, DateTime.Now),
                    "application/octet-stream", "Заява_голові_ОСББ.docx");
        }

        public ActionResult Phones(int id)
        {
            return PartialView(OwnerHelper.GetPhonesById(id));
        }

        public ActionResult PropertiesOwner(int id)
        {
            return PartialView(OwnerHelper.GetPropertiesByID(id));
        }

        public ActionResult OwnersHead()
        {
            //для пошуку по будинку та квартирі
            int selectedIndex = 1;
            SelectList houses = new SelectList(PropertyHelper.GetHousesByIdOSBB(OwnerHelper.GetIdOSBBByIdOwner(int.Parse(Session["user_id"].ToString()))), "HouseID", "Name", selectedIndex);
            ViewBag.Houses = houses;
            SelectList rooms = new SelectList(PropertyHelper.GetRoomsByIdHouse(selectedIndex), "RoomID", "Name");
            ViewBag.Rooms = rooms;

            SelectList types = new SelectList(OwnerHelper.GetTypesOfOwners(), "TypeOwnerID", "Name");
            ViewBag.Types = types;

            Owner owner = OwnerHelper.GetOwnerById(Convert.ToInt32(Session["user_id"].ToString()));
            return View(OwnerHelper.GetAllOwners(owner.OOABID));
        }

        public ActionResult Owners()
        {
            //для пошуку по будинку та квартирі
            int selectedIndex = 1;
            SelectList houses = new SelectList(PropertyHelper.GetHousesByIdOSBB(OwnerHelper.GetIdOSBBByIdOwner(int.Parse(Session["user_id"].ToString()))), "HouseID", "Name", selectedIndex);
            ViewBag.Houses = houses;
            SelectList rooms = new SelectList(PropertyHelper.GetRoomsByIdHouse(selectedIndex), "RoomID", "Name");
            ViewBag.Rooms = rooms;

            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            return View(OwnerHelper.GetAllActiveOwners(id_osbb));
        }

        public ActionResult SearchOwners(int id_osbb, string surname, string name)
        {
            List<Owner> searchedowners;
            searchedowners = OwnerHelper.SearchOwnersBySurnameName(id_osbb, surname, name);
            return PartialView(searchedowners);
        }

        public ActionResult SearchOwnersByHouseRoomHelper(int id)
        {
            return PartialView(PropertyHelper.GetRoomsByIdHouse(id).ToList());
        }

        public ActionResult SearchOwnersByHouseRoom(int room)
        {
            return PartialView("SearchOwners", OwnerHelper.SearchOwnersByHouseRoom(room).ToList());
        }

        public ActionResult DoNoActive(int id)
        {
            OwnerHelper.MakeNotActive(id);
            Owner owner = OwnerHelper.GetOwnerById(Convert.ToInt32(Session["user_id"].ToString()));
            return RedirectToAction("OwnersHead");
        }

        public ActionResult DoActive(int id)
        {
            OwnerHelper.MakeActive(id);
            Owner owner = OwnerHelper.GetOwnerById(Convert.ToInt32(Session["user_id"].ToString()));
            return RedirectToAction("OwnersHead");
        }

        public ActionResult EditOwner(int id_owner)
        {
            SelectList types = new SelectList(OwnerHelper.GetTypesOfOwners(), "TypeOwnerID", "Name");
            ViewBag.Types = types;
            return View(OwnerHelper.GetOwnerById(id_owner));
        }

        public ActionResult SaveChangeOwner(Owner owner)
        {
            if(ModelState.IsValid)
            {
                OwnerHelper.UpdateChangeOwner(owner);
                ViewBag.Result = "Інформація збережена!";        
                return View("AdditionSuccessful");
            }
            SelectList types = new SelectList(OwnerHelper.GetTypesOfOwners(), "TypeOwnerID", "Name");
            ViewBag.Types = types;
            return View("EditOwner", OwnerHelper.GetOwnerById(owner.OwnerID));
        }
    }
}
