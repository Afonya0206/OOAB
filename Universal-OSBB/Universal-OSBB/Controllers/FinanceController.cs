﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Universal_OSBB.Models.Entities;
using Universal_OSBB.Models.Entities.Reports;
using Universal_OSBB.Models.Repository;

namespace Universal_OSBB.Controllers
{
    public class FinanceController : Controller
    {
        public ActionResult CostPlanning()
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            return View(FinanceHelper.GetFutureCostPlanning(id_osbb));
        }
        public ActionResult CostPlanningOwners()
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            return View(FinanceHelper.GetFutureCostPlanning(id_osbb));
        }

        public ActionResult PaymentForRooms()
        {
            List<Receipt> receipts = FinanceHelper.GetReceiptsForRooms(OwnerHelper.GetIdOSBBByIdOwner(int.Parse(Session["user_id"].ToString())), int.Parse(Session["user_id"].ToString()));
            decimal total = 0;
            for (int i = 0; i < receipts.Count(); i++)
                total += receipts[i].Payment;
            ViewBag.Total = total;
            return View(receipts);
        }

        public ActionResult TotalPaymentForRoom(int id_room, int id_osbb, int id_owner)
        {
            decimal TotalPaymentForRoom = FinanceHelper.TotalPaymentByIdRoom(id_room, id_osbb, id_owner);
            ViewBag.TotalForRoom = TotalPaymentForRoom;
            return PartialView();
        }

        public ActionResult CreatePlannedProduct()
        {
            FillViewBagLists();
            return View();
        }

        public ActionResult EditPlannedProduct(int id_product)
        {
            FillViewBagLists();
            return View(FinanceHelper.GetProductById(id_product));
        }

        public ActionResult DetailsPlannedProduct(int id_product)
        {
            return View(FinanceHelper.GetProductById(id_product));
        }   

        public ActionResult DeletePlannedProduct(int id_product)
        {
            FinanceHelper.DeleteProductById(id_product);
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            return View("CostPlanning", FinanceHelper.GetFutureCostPlanning(id_osbb));    

        }

        public ActionResult SaveEditPlannedPurchase(Cost product)
        {
            if (product.DateEnd != null && product.DatePurchase > product.DateEnd)
                ModelState.AddModelError("DatePurchase", "Оберіть коректні межі періоду придбання!");
            if (ModelState.IsValid)
            {
                if (product.DatePurchase != null)
                {
                    DateTime now = DateTime.Now;
                    DateTime min_date;
                    if (now.Month == 12)
                        min_date = new DateTime(now.Year + 1, 1, 1);
                    else
                        min_date = new DateTime(now.Year, now.Month + 1, 1);

                    if (product.DatePurchase < min_date)
                        ModelState.AddModelError("DatePurchase", "Планувати закупівлі можна тільки на майбутні періоди " + 
                            "(не раніше, ніж з першого числа наступного місяця)!");
                }
                if (ModelState.IsValid)
                {
                    FinanceHelper.UpdateProduct(product);
                    int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
                    ViewBag.Result = "Інформація про заплановані витрати збережена!";
                    return View("AdditionSuccessful");
                }
                else
                {
                    FillViewBagLists();
                    return View("EditPlannedProduct", product);
                }
            }
            else
            {
                FillViewBagLists();
                return View("EditPlannedProduct", product);
            }

        }
        

        public ActionResult AddPlannedPurchase(Cost product)
        {
            if (product.DateEnd != null && product.DatePurchase > product.DateEnd)
                ModelState.AddModelError("DatePurchase", "Оберіть коректні межі періоду придбання!");
            if (ModelState.IsValid)
            {
                DateTime now = DateTime.Now;
                DateTime min_date;
                if (now.Month == 12)
                    min_date = new DateTime(now.Year + 1, 1, 1);
                else
                    min_date = new DateTime(now.Year, now.Month + 1, 1);

                if (product.DatePurchase < min_date)
                    ModelState.AddModelError("DatePurchase", "Планувати закупівлі можна тільки на майбутні періоди " + 
                        "(не раніше, ніж з першого числа наступного місяця)!");
                
                if (ModelState.IsValid)
                {
                    product.OOABID = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
                    FinanceHelper.AddPurchasedProduct(product);
                    ViewBag.Result = "Інформація про заплановані витрати збережена!";
                    return View("AdditionSuccessful");
                }
                else
                {
                    FillViewBagLists();
                    return View("CreatePlannedProduct", product);
                }
            }
            else
            {
                FillViewBagLists();
                return View("CreatePlannedProduct", product);
            }
        }

        public void FillViewBagLists()
        {
            List<Measurement> measurements = FinanceHelper.GetMeasurements();
            SelectList list_measurements = new SelectList(measurements, "MeasurementID", "Name");
            if (list_measurements.Count() > 0)
                list_measurements.First().Selected = true;
            ViewBag.Measurements = list_measurements;
        }
        public ActionResult SpentMoneySearch()
        {
            return View();
        }

        public ActionResult AllSpentMoney()
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            List<Cost> spent_money = FinanceHelper.GetSpentMoney(id_osbb, null, null);
            ViewBag.TotalSum = FinanceHelper.TotalSpentMoney(spent_money);
            return View("SpentMoney", spent_money);
        }

        public ActionResult SpentMoney(SpentMoneyReport dates)
        {
            if (dates.DateFrom != null && dates.DateTo != null && dates.DateFrom > dates.DateTo)
                ModelState.AddModelError("DateFrom", "Оберіть коректні межі звітного періоду!");
            if (ModelState.IsValid)
            {
                int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));

                List<Cost> spent_money = new List<Cost>();

                ViewBag.Message = "в період з " + dates.DateFrom.Date.ToString("d") + " до " + dates.DateTo.Date.ToString("d");
                spent_money = FinanceHelper.GetSpentMoney(id_osbb, dates.DateFrom, dates.DateTo);
                ViewBag.TotalSum = FinanceHelper.TotalSpentMoney(spent_money);

                return View(spent_money);
            }
            else
                return View("SpentMoneySearch");
        }

        public ActionResult MonthlyFee()
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            return View(FinanceHelper.MonthlyFeesBy12(id_osbb, DateTime.Now.Month, DateTime.Now.Year));
        }

        public ActionResult CreateReceipt()
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
  
            List<House> houses = PropertyHelper.GetHousesByIdOSBB(id_osbb);
            SelectList list_houses = new SelectList(houses, "HouseID", "Name");
            ViewBag.Houses = list_houses;
            
            SelectList rooms = new SelectList(PropertyHelper.GetRoomsByIdHouse(houses.First().HouseID), "RoomID", "Name");
            ViewBag.Rooms = rooms;

            return View();
        }

        public ActionResult ListRooms(int id)
        {
            return PartialView(PropertyHelper.GetRoomsByIdHouse(id));
        }

        public ActionResult AddReceipt(Receipt receipt)
        {
            if(receipt.RoomID == 0)
            {
                ModelState.AddModelError("RoomID", "Оберіть приміщення!");
            }
            if (receipt.DatePayment > DateTime.Now)
            {
                ModelState.AddModelError("DatePayment", "Дата сплати не повинна перевищувати сьогоднішню дату!");
            }
            if (ModelState.IsValid)
            {

                FinanceHelper.AddReceipt(receipt);
                ViewBag.Result = "Інформація про сплату коштів збережена!";
                return View("AdditionSuccessful");
            }
            else
            {
                int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
                List<House> houses = PropertyHelper.GetHousesByIdOSBB(id_osbb);
                SelectList list_houses = new SelectList(houses, "HouseID", "Name");
                ViewBag.Houses = list_houses;

                SelectList rooms = new SelectList(PropertyHelper.GetRoomsByIdHouse(houses.First().HouseID), "RoomID", "Name");
                ViewBag.Rooms = rooms;
                return View("CreateReceipt");
            }
        }

        public ActionResult CurrentCosts()
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            return View(FinanceHelper.GetCurrentCostPlanning(id_osbb));
        }

        [HttpPost]
        public ActionResult SaveCurrentCosts(List<Cost> current_costs)
        {
            FinanceHelper.UpdatePerform(current_costs);
            ViewBag.Result = "Інформація збережена!";
            return View("AdditionSuccessful");
        }

        public ActionResult DetailsCurrentCost(int id_product)
        {
            return View(FinanceHelper.GetProductById(id_product));
        }

        public ActionResult Deptors()
        {
            int id_osbb = OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()));
            return View(FinanceHelper.GetDeptors(id_osbb));
        }

        public ActionResult DeptorsOwners(int id_room)
        {

            return PartialView(PropertyHelper.GetOwnersById(id_room));
        }

        
    }
}
