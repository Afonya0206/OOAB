﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Universal_OSBB.Models.Entities;
using Universal_OSBB.Models.Entities.Registration;
using Universal_OSBB.Models.Repository;
using Universal_OSBB.Mappers;

namespace Universal_OSBB.Controllers
{
    public class UserController : Controller
    {
        [HttpGet]
        public ActionResult Registration()
        {
            List<TypeOwner> types = OwnerHelper.GetTypesOfOwners();
            ViewBag.Types = types;
            return View();
        }

        [HttpPost]
        public ActionResult RegisterUser(RegisterUser user_reg, List<string> phones, string type_owner,
            string newvalue)
        {
            if (!OwnerHelper.IsValidEmail(user_reg.Email))
            {
                ModelState.AddModelError("Email", "Користувач з такою електронною адресою вже зареєстрований!");
            }

            if (!OwnerHelper.IsValidLogin(user_reg.Login))
            {
                ModelState.AddModelError("Login", "Користувач з таким логіном вже зареєстрований!");
            }

            CommonMapper ModelMapper = new CommonMapper();
            Owner user = (Owner)ModelMapper.Map(user_reg, typeof(RegisterUser), typeof(Owner));

            if (type_owner == "new" && newvalue == "")
            {
                ModelState.AddModelError("TypeOwner", "Введіть посаду співвласника!");
            }

            bool flag = false;
            foreach(string s in phones)
            {
                if (s == "")
                    flag = true;
            }

            if(flag)
            {
                ModelState.AddModelError("TypeOwner", "Введіть всі телефони співвласника!");
            }

            if (ModelState.IsValid)
            {
                int id_type;
                if(type_owner == "new")
                {
                    id_type = OwnerHelper.AddNewType(newvalue);
                }
                else
                {
                    id_type = Convert.ToInt32(type_owner);
                }

                Owner current_owner = OwnerHelper.GetOwnerById(Convert.ToInt32(Session["user_id"].ToString()));
                UserHelper.AddOwner(user, phones, current_owner.OOABID, id_type);
                return View("RegistrationSuccessful");
            }
            else
            {
                List<TypeOwner> types = OwnerHelper.GetTypesOfOwners();
                ViewBag.Types = types;
                return View("Registration");
            }
        }

        public ActionResult LogIn(string login, string password)
        {
            int? id = UserHelper.IsValidLoginAndPassword(login, password);
            
            if(id != null)
            {
                Session["user_id"] = id.ToString();
                return RedirectToAction("News", "Novelty"); 
            }
            else
                TempData["ResultLogIn"] = "Не існує користувача з таким логіном та паролем";
            return RedirectToAction("Index", "Home");           
        }
        public ActionResult LogOut()
        {
            Session["user_id"] = null;
            return RedirectToAction("Index", "Home");
        }

        public ActionResult UserInfo()
        {
            Owner user = OwnerHelper.GetOwnerById(Convert.ToInt32(Session["user_id"].ToString()));
            return PartialView(user);           
        }

        public ActionResult ForgetPassword()
        {
            TempData["Form"] = "Forget";
            return RedirectToAction("Index", "Home");
        }
        public ActionResult SendNewPassword(string mail)
        {
            Owner owner = OwnerHelper.GetOwnerByEmail(mail);
            if (owner == null)
            {
                TempData["Form"] = "Forget";
                TempData["ResultEnterEmail"] = "Не існує користувача з такою адресою";
                return RedirectToAction("Index", "Home");
            }
            else
            {
                UserHelper.ChangePassword(owner);
                TempData["Successful"] = "Новий пароль відправлено на електронну пошту " + owner.Email;
                return RedirectToAction("Index", "Home");
            }
        }

    }
}
