﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Universal_OSBB.Models.Entities;
using Universal_OSBB.Models.Repository;

namespace Universal_OSBB.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MenuHead()
        {
            return PartialView();
        }

        public ActionResult MenuOwner()
        {
            return PartialView();
        }

        public ActionResult SubMenuHead()
        {
            return PartialView();
        }

        public ActionResult SubMenuOwner()
        {
            return PartialView();
        }

        public ActionResult LastNews()
        {
            return PartialView(NewsHelper.GetLastFiveNews(OwnerHelper.GetIdOSBBByIdOwner(Convert.ToInt32(Session["user_id"].ToString()))));
        }

        public ActionResult FooterHead()
        {
            Owner owner = OwnerHelper.GetOwnerById(Convert.ToInt32(Session["user_id"].ToString()));
            return PartialView(OwnerHelper.GetResponsibleOwners(owner.OOABID));
        }

        public ActionResult FooterOwner()
        {
            int id_user = Convert.ToInt32(HttpContext.Session["user_id"].ToString());
            Owner user = OwnerHelper.GetOwnerById(id_user);
            Owner head = OwnerHelper.GetHeadOfOOAB(user.OOABID);
            ViewBag.Phones = OwnerHelper.GetPhonesById(head.OwnerID);
            return PartialView(head);
        }
    }
}
