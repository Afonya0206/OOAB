﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Universal_OSBB.Models.Entities;
using Universal_OSBB.Models.Entities.Registration;

namespace Universal_OSBB.Mappers
{
    public class CommonMapper: IMapper
    {
        static CommonMapper()
        {
            Mapper.CreateMap<Owner, RegisterUser>();

            Mapper.CreateMap<RegisterUser, Owner>();
        }

        public object Map(object source, Type source_type, Type destination_type)
        {
            return Mapper.Map(source, source_type, destination_type);
        }
    }
}