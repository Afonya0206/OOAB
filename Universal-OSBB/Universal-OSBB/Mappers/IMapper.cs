﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Universal_OSBB.Mappers
{
    interface IMapper
    {
        object Map(object source, Type source_type, Type destination_type);
    }
}
